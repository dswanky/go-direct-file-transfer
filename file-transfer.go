package main

import ("fmt"
	"net"
  "io"
  "strings"
  "os"
  "time"
  "encoding/json"
  "path/filepath"
	"encoding/binary"
  //"image/color"
  "gioui.org/app"
  "gioui.org/op"
  //"gioui.org/text"
  //"gioui.org/unit"
  "gioui.org/layout"
  "gioui.org/widget"
  "gioui.org/widget/material"
  "gioui.org/x/explorer")

type connectionHandler func(net.Conn, map[string]string)
type readMessageHandler func(message message, bytesRead uint64)
type message struct{
  
  size uint64
  chunkSize uint64
  params map[string]string
  chunkHandler readMessageHandler
	stream net.Conn
  
}

var sizeByteLen = 64/8
var maxChunk = 1024 * 1024 * 8//8MB
var globWriter net.Conn
var globSock net.Conn

func readSize(reader net.Conn) uint64{

	sizeBytes := make([]byte, sizeByteLen)
	reader.Read(sizeBytes)
  byteNum := binary.BigEndian.Uint64(sizeBytes)
  return byteNum
  
}

func readMessage(reader net.Conn, dataHandler readMessageHandler, params map[string]string){

  newParams := make(map[string]string)
  readJSON(reader, &newParams)
  fmt.Println("NEW PARAMS:", newParams)
  for k, v := range newParams{
    params[k] = v
  }
	byteNum := readSize(reader)
  fmt.Println("Read size of: ", byteNum)
  
	dataHandler(
    message{
      byteNum,
      uint64(maxChunk),
      params,
      dataHandler,
      reader},
    0)
	
}

func writeFile(message message, bytesRead uint64){
  fmt.Println("WRITING FILE:", message.params["filename"] + "-out")
  splitFileName := strings.Split(message.params["filename"], "/")
  if len(splitFileName) <= 1 {
    splitFileName = strings.Split(message.params["filename"], "\\")
  }
  onlyFileName := splitFileName[len(splitFileName)-1]
	f,_ := os.OpenFile(onlyFileName + "-out", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0700)
  //defer f.Close()
  reader := message.stream
  
  for {

    readSize := message.chunkSize

    if message.size - bytesRead < readSize {readSize = message.size - bytesRead}

    fmt.Println("SERVER READ SIZE:", readSize)
    chunk := make([]byte, readSize)
    fmt.Println("SERVER CHUNK SIZE:", len(chunk))
    bRead, err := reader.Read(chunk)
    fmt.Println("SERVER ACTUAL READ SIZE:", bRead)
    
    if bRead == 0{
      //fmt.Println(globWriter)
      //globWriter.Flush()
      time.Sleep(1 * time.Second)
    }
    
    if err != nil{

      fmt.Println("WRITE FILE FUNC ERROR: ", err)

      if err == io.EOF{
        
        fmt.Println("Finished writing file")
        return
        
        
      }
      
    }
    
    //fmt.Println("Read from stream: ", chunk)
    f.Write(chunk[0:bRead])

    bytesRead += uint64(bRead)
    fmt.Println("SERVER READ TOTAL:", bytesRead)
    if bytesRead >= message.size {
      fmt.Println("Read", bytesRead, "bytes, done.")
      return
    }
    
  }
	
}



func sendMessage(writer net.Conn, messageHandler readMessageHandler, params map[string]string){

  messageHandler(message{
    0,
    uint64(maxChunk),
    params,
    messageHandler,
    writer},
    0)
  
}

func sendSize(writer net.Conn, size uint64){

  returnVal := make([]byte, 8)
  binary.BigEndian.PutUint64(returnVal, size)
  fmt.Println("Sending file size: ", size, ", ", returnVal)
  writer.Write(returnVal)
  
}

func readJSON(reader net.Conn, v any) any{

  size := readSize(reader)
  fmt.Println("RECEIVE READSIZE:", size)
  jsonBytes := make([]byte, size)
  reader.Read(jsonBytes)
  json.Unmarshal(jsonBytes, &v)
  fmt.Println("RECEIVING:", v)
  return v
  
}

func sendJSON(writer net.Conn, v any){

  bytes, _ := json.Marshal(v)
  size := len(bytes)
  sendSize(writer, uint64(size))
  fmt.Println("SENDING:",v)
  writer.Write(bytes)
  
}

func readFile(message message, size uint64){

  f,_ := os.OpenFile(message.params["filename"], os.O_RDONLY, 0000)
  writer := message.stream
  fStats, _ := f.Stat()
  fSize := fStats.Size()
  sendJSON(writer, message.params)
  sendSize(writer, uint64(fSize))
  totalSent := 0

  for {

    chunk := make([]byte, message.chunkSize)
    bRead, _ := f.Read(chunk)
    //bRead := len(chunk)
    //fmt.Println("Read ", bRead, " bytes from file.")
    if bRead == 0 {
      fmt.Println("CLIENT EOF, stopping file read")
      //writer.Flush()
      return
    }
    
    //fmt.Println("Sending data: ", chunk[0:bRead])
    fmt.Println("CLIENT Writing", bRead, "bytes")
    //fmt.Println(chunk)
    
    _, err := message.stream.Write(chunk[0:bRead])

    //_, err := writer.Write(chunk[0:8000000])

    totalSent += bRead
    fmt.Println("CLIENT Total bytes sent: ", totalSent)    
    
    if err != nil {
      
      fmt.Println("CLIENT READ FILE FUNC ERROR: ", err)


      if strings.Contains(err.Error(), "timeout"){

        fmt.Println("CLIENT SETTING NEW DEADLINE")
        writer.SetWriteDeadline(time.Now().Add(5 * time.Second))
        
      }
      
    }
    
    
  }
  
}

func connHandler(conn net.Conn, params map[string]string){

	//defer conn.Close()
  fmt.Println("handling server")
	//reader := bufio.NewReader(conn)
  //writer := bufio.NewWriter(conn)
	
	readMessage(conn, writeFile, params)
  fmt.Println("Closing server")
	
}

func listenForConn(callback connectionHandler, params map[string]string) {

  fmt.Println("Creating server")
	listener,err := net.Listen("tcp", ":8182")
	if err != nil {

		fmt.Println(err)
		return
		
	}

	for {
		
		conn, err := listener.Accept()

		if err != nil {

			fmt.Println(err)
			continue
			
		}

		callback(conn, params)
    fmt.Println("AFTER CONN")
		
		
	}
	
	
}

func clientHandler(conn net.Conn, params map[string]string){

  //defer conn.Close()
  fmt.Println("Handling Client")
  time.Sleep(1 * time.Second)
  sendMessage(conn, readFile, params)
  fmt.Println("Closing client")
  time.Sleep(1 * time.Second)
  
}

func createClient(address string, callback connectionHandler, params map[string]string){

  fmt.Println("Creating Client")
  conn, _ := net.Dial("tcp", address)
  conn.SetWriteDeadline(time.Now().Add(5 * time.Second))
  callback(conn, params)
  fmt.Println("ENDING CLIENT")
  
  
}

func sendFile(address string, filename string){

  createClient(address, clientHandler, map[string]string{"filename":filename})
  
}

func graphicsLoop(){

  window := new(app.Window)
  run(window)
  
}

func fileInput(gtx layout.Context, theme *material.Theme,
  wInput *widget.Editor, wButton  *widget.Clickable,
  wInputText string, wButtonText string) layout.Dimensions {
  return layout.Flex{
    
    Axis: layout.Horizontal,
    Spacing: layout.SpaceBetween,
    
  }.Layout(gtx, layout.Flexed(0.9, func(gtx layout.Context) layout.Dimensions {
    return material.Editor(theme, wInput, wInputText).Layout(gtx)
  }),
    layout.Flexed(0.1, func(gtx layout.Context) layout.Dimensions {
      button := material.Button(theme, wButton, wButtonText)
      
      return button.Layout(gtx)
    }))
  
}

func run(window *app.Window){

  theme := material.NewTheme()
  var ops op.Ops
  ipInput := new(widget.Editor)
  chooseFile := new(widget.Editor)
  chooseFileButton := new(widget.Clickable)
  startButton := new(widget.Clickable)
  
  for{

    switch e := window.Event().(type) {

    case app.DestroyEvent:
      return
    case app.FrameEvent:
      gtx := app.NewContext(&ops, e)
      
      if chooseFileButton.Clicked(gtx) {
        fileCloser, err := explorer.NewExplorer(window).ChooseFile()
        //Gotta assert that this is a file, so it won't work on web or mobile
        //Which is file in this case
        filePath, _ := filepath.Abs(fileCloser.(*os.File).Name())
        chooseFile.SetText(filePath)
        fmt.Println("CLICK", err)
      }

      if startButton.Clicked(gtx) {
        address := ipInput.Text()
        inFile := chooseFile.Text()
        sendFile(address, inFile)
        
        
      }
      
      layout.Flex{

        Axis: layout.Vertical,
        Spacing: layout.SpaceEnd,
        
      }.Layout(gtx,
        layout.Rigid(
          func(gtx layout.Context) layout.Dimensions {
            return material.Editor(theme, ipInput, "Format: IP:PORT").Layout(gtx)
          }),
        layout.Rigid(
          func(gtx layout.Context) layout.Dimensions {
            return fileInput(gtx, theme, chooseFile, chooseFileButton, "Select File", "Open...")
          },
        ),
        layout.Rigid(func(gtx layout.Context) layout.Dimensions {

          return material.Button(theme, startButton, "Transfer").Layout(gtx)
          
        },),
      )
      
      e.Frame(gtx.Ops)
      
    }
    
  }
  
}

func main(){


  //go listenForConn(connHandler)
  //go createClient("localhost:8182", clientHandler)
  go listenForConn(connHandler, map[string]string{})
  //time.Sleep(10000 * time.Second)
  graphicsLoop()

    
}
